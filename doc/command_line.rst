FUNPACK command-line interface
==============================


FUNPACK is controlled entirely through its command-line interface. You can
control which data fields and subjects to import, which cleaning/processing
rules to apply, and how the output should be formatted.


Configuration files
-------------------

FUNPACK can also be controlled through configuration files - a FUNPACK
configuration file simply contains a list of command-line options, without the
leading ``--``. For example, the options in the following command-line::


    fmrib_unpack                     \
      --overwrite                    \
      --write_log                    \
      --icd10_map_file icd_codes.tsv \
      --category 10                  \
      --category 11                  \
      output.tsv input1.tsv input2.tsv


Could be stored in a configuration file ``config.txt``::

    overwrite
    write_log
    icd10_map_file icd_codes.tsv
    category       10
    category       11


And then executed as follows::

    fmrib_unpack -cfg config.txt output.tsv input1.tsv input2.tsv


A FUNPACK configuration file can *include* other configuration files, making
it possible to build a *configuration proflie* which is organised across
several configuration files. For example, you may wish to store variable
selectors in one file ``variables.cfg``::

    variable 31    # sex
    variable 33    # dob
    variable 41202 # ICD10 main diagnoses

Then your main configuration file can include ``variables.cfg`` via the
``config_file`` argument, along with any other options::

    overwrite
    write_log
    config_file variables.cfg

The FMRIB configuration profile (described :ref:`here
<fmrib_configuration_profile>`) uses this feature to organise the options and
rules that it applies.


Command-line reference
----------------------


All of the command-line options that FUNPACK accespts are documented below:

..
  Defined in conf.py

.. codesub:: none

   |funpack_cli_help|
