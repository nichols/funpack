#!/usr/bin/env fslpython
#
# sphinx extensions which:
#    - embed content wihtin a HTML <details> element
#    - allows subsitutions in paths passed to include::
#    - allows subsitutions in codeblock::


from docutils                import nodes
from docutils.parsers.rst    import Directive
from sphinx.directives.other import Include
from sphinx.directives.code  import CodeBlock
from sphinx.util.nodes       import nested_parse_with_titles


class DetailsNode(nodes.Element):
    pass

class SummaryNode(nodes.Element):
    pass

class DetailsDirective(Directive):
    has_content = True
    def run(self):
        node = DetailsNode()
        nested_parse_with_titles(self.state, self.content, node)
        return [node]

class SummaryDirective(Directive):
    has_content = True
    def run(self):
        node = SummaryNode()
        self.state.nested_parse(self.content, self.content_offset, node)
        return [node]

def visitSummaryNode(self, node):
    self.body.append('<details><summary>')

def departSummaryNode(self, node):
    self.body.append('</summary>')

def visitDetailsNode(self, node):
    self.body.append('<p>')

def departDetailsNode(self, node):
    self.body.append("</p></details>")

class SubstitutionInclude(Include):
    def run(self):
        filename = self.arguments[0]
        for orig, repl in self.state.document.substitution_defs.items():
            filename = filename.replace(f'|{orig}|', repl.astext())
        self.arguments[0] = filename
        return super().run()


class SubstitutionCode(CodeBlock):
    def run(self):
        newcontent = []
        for line in self.content:
            for orig, repl in self.state.document.substitution_defs.items():
                line  = line.replace(f'|{orig}|', repl.astext())
            # hack to allow multi-line substitutions
            line = line.replace('//n//', '\n')
            newcontent.append(line)
        self.content = newcontent
        return super().run()

def setup(app):
    app.add_node(DetailsNode, html=(visitDetailsNode, departDetailsNode))
    app.add_node(SummaryNode, html=(visitSummaryNode, departSummaryNode))
    app.add_directive('details',    DetailsDirective)
    app.add_directive('summary',    SummaryDirective)
    app.add_directive('includesub', SubstitutionInclude)
    app.add_directive('codesub',    SubstitutionCode)
