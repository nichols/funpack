FUNPACK
=======

.. toctree::
   :hidden:
   :maxdepth: 1

   self
   demo.ipynb
   function_reference
   command_line
   fmrib_profile
   apiref
   changelog

.. include:: ../README.rst
