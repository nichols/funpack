FUNPACK API reference
=====================


FUNPACK is written in the Python programming language. While FUNPACK was
designed primarily as a command-line tool, it can also be programmatically
called from a Python script. The pages in this section document all of the
classes and functions that are contained within the FUNPACK codebase.

.. toctree::
   :maxdepth: 0

   funpack.cleaning
   funpack.cleaning_functions
   funpack.config
   funpack.custom
   funpack.datatable
   funpack.dryrun
   funpack.exporting
   funpack.exporting_hdf5
   funpack.exporting_tsv
   funpack.fileinfo
   funpack.icd10
   funpack.importing.core
   funpack.importing.filter
   funpack.importing.reindex
   funpack.loadtables
   funpack.main
   funpack.merging
   funpack.metaproc_functions
   funpack.parsing.process
   funpack.parsing.value_expression
   funpack.parsing.variable_expression
   funpack.plugins.fmrib
   funpack.processing
   funpack.processing_functions
   funpack.processing_functions_core
   funpack.schema
   funpack.schema.coding
   funpack.schema.hierarchy
   funpack.scripts.demo
   funpack.util
