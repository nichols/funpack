The files in this directory contain metadata about the data fields present in
UK BioBank data sets. These files were downloaded from the UK BioBank online
data showcase (https://biobank.ctsu.ox.ac.uk/crystal/schema.cgi).


In normal usage, FUNPACK does not use these files at all - when FUNPACK is
executed, it attempts to download the latest vesrions of all required metadata
files. The files in this directory are only used as a back-up in case the
files cannot be downloaded.


These back-up files are updated on each new FUNPACK release (see the
funpack/scripts/refresh_showcase_schema.py file).
