#!/bin/bash

pip install --upgrade pip setuptools
pip install -r requirements.txt
pip install pylint flake8
flake8                           funpack --exclude=funpack/tests || true
pylint --output-format=colorized funpack --ignore=funpack/tests  || true
