#!/usr/bin/env bash

set -e

apt-get update -y
apt-get install -y bsdmainutils pandoc

pip install -r requirements.txt
pip install -r requirements-demo.txt
pip install -r requirements-test.txt
pip install .

python setup.py doc

cp -r doc/html public
