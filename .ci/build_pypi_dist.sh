#!/usr/bin/env bash

set -e

apt-get update -y
apt-get install -y bsdmainutils

pip install wheel
python setup.py sdist
python setup.py bdist_wheel

# do a test install from both source and wheel
sdist=`find dist -maxdepth 1 -name *.tar.gz`
wheel=`find dist -maxdepth 1 -name *.whl`


for target in $sdist $wheel; do
    python -m venv test.venv
    . test.venv/bin/activate
    pip install --upgrade pip setuptools
    pip install --only-binary tables tables
    pip install $target
    pip install -r requirements-demo.txt
    pip install -r requirements-test.txt
    pushd / > /dev/null
    python -c "import funpack; print(funpack.__version__)"
    fmrib_unpack -V
    pytest -W "ignore::pandas.errors.ParserWarning" --pyargs funpack.tests
    deactivate
    popd > /dev/null
    rm -r test.venv
done
